# INSTALL AND DEFINE SERVICES
package 'apache2'
package 'htop'
package 'mc'
package 'wget'
package 'curl'
package 'git'
package 'nagios-plugins'
package 'nagios-nrpe-server'
package 'sysv-rc-conf' if node['platform_family'] == 'debian'


# PATCH /etc/inputrc
cookbook_file '/etc/inputrc' do
   mode '0644'
   owner 'root'
   group 'root'
end

# Midnight Commander dark theme
directory '/root/.local/share/mc/skins' do
	mode '0755'
	owner 'root'
	group 'root'
	action :create
	recursive true
end

cookbook_file '/root/.local/share/mc/skins/ajnasz-blue.ini' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'ajnasz-blue.ini'
end

# Remote agent for Sublime Text
execute "Download and make executable rsub" do
	user "root"
	command "wget -O /usr/local/bin/rsub https://raw.github.com/aurora/rmate/master/rmate && chmod +x /usr/local/bin/rsub"
	not_if { ::File.exist?('/usr/local/bin/rsub') }
end

execute "Download latest docker for ubuntu" do
	command 'wget -qO- https://get.docker.io/gpg | sudo apt-key add - && sh -c "echo deb http://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list" && apt-get update && apt-get -yq upgrade && apt-get -yq install lxc-docker'
	user "root"
	not_if { ::File.exist?('/usr/bin/docker') }
end


#################################################################
# Automation of docker install and configuration.
service 'docker' do
  supports :reload => true
  action :start
end

execute "docker pull and run local registry server" do
	command "docker run -p 5000:5000 -d registry"
	user "root"
end

execute "docker download icinga2 image" do
	command "docker pull jordan/icinga2"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end


# Create temp dir for files
directory '/tmp/icinga_demo/roothost' do
	mode '0755'
	owner 'root'
	group 'root'
	action :create
	recursive true
end

cookbook_file '/tmp/icinga_demo/inputrc' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/icinga_demo/ajnasz-blue.ini' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/icinga_demo/Dockerfile' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/icinga_demo/nrpe.cfg' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/icinga_demo/nrpe.cfg' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'nrpe.cfg'
end

service 'nagios-nrpe-server' do
  supports :reload => true
  action :restart
end

cookbook_file '/etc/apache2/sites-available/proxypass2icinga.conf' do
	mode '0640'
	owner 'root'
	group 'root'
end

execute "make symlink to enable proxypass2icinga" do
	user "root"
	command "cd /etc/apache2/sites-enabled && ln -s ../sites-available/proxypass2icinga.conf proxypass2icinga.conf"
	not_if { ::File.exist?('/usr/local/bin/rsub') }
end

service 'apache2' do
  supports :reload => true
  action :restart
end


# add files to /etc/icinga2/conf.d/hosts/roothost/ && roothost.conf
cookbook_file '/tmp/icinga_demo/roothost.conf' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'roothost.conf'
end
remote_directory "/tmp/icinga_demo/roothost" do
	files_mode '0640'
	files_owner 'root'
	mode '0775'
	owner 'root'
	source 'roothost'
end

execute "customize docker's icinga image" do
	command "docker build -t icinga_demo:v1 /tmp/icinga_demo"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

execute "run container with icinga2" do
	command "docker run -t icinga_demo:v1 &"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

execute "remove dir with temp files" do
	command "rm -fr /tmp/icinga_demo"
	user "root"
end

# save docker image to local registry server
execute "save docker image to local registry server" do
	command "docker tag icinga_demo:v1 localhost:5000/icinga_demo:v1 && docker push localhost:5000/icinga_demo:v1"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

